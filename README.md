<img src="https://i.imgur.com/89hGVic.png" height="50%" width="50%;"/>
  
# ABCrom Modified#
  
[Android Builders Collective](https://forum.xda-developers.com/custom-roms/android-builders-collective/rom-builders-collective-t2861778)

  
### About: ###
This is a version of ABCrom that I build for shamu.  You can find compiled builds on my [AFH page](https://www.androidfilehost.com/?a=show&w=files&flid=153215).
  
ABCrom is maintained by ezio84.  You can find the main ABCrom manifest [here](https://github.com/ezio84/abc_manifest).
  
### Sync: ###
```bash
repo init -u https://gitlab.com/abcmod/abc_manifest.git -b n2 --no-clone-bundle
repo sync --force-sync -c --no-clone-bundle --no-tags --optimized-fetch --prune
```
  


